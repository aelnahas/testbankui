import React from "react";

const TestProjectHeader = ({name, description, testCount}) => (
  <div className="test-project_header">
    <label className="test-project_header__name">{name}</label>
    <label className="test-project_header__description">{description}</label>
    <label className="test-project_header__count">{`${ testCount } Tests`}</label>
  </div>
);

export default TestProjectHeader;