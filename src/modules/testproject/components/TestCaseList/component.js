import React, { Component } from "react";
import TestClassRow from "../TestCaseRow/component";

class TestCaseList extends Component {
  render() {
    const testCases = this.props.testCases.map( testCase => (
      <TestClassRow testCase={testCase} key={testCase.id}/>
    ));

    return (
      <div className="test-cases">
        {testCases}
      </div>
    );
  }
}

export default TestCaseList;