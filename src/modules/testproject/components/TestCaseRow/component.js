import React, { Component } from "react";
import { Button, Card, Checkbox } from "antd";
import { Row, Col } from "antd";
import "./syle.scss";

class TestClassRow extends Component {
  render() {
    const { name } = this.props.testCase.attributes;
    return (
      <div className="testcase-row-container">
        <Card className="testcase-row-card">
          <Row className="test-case_row__content">
            <Col span={6}><Checkbox/></Col>
            <Col span={6}>
              <label className="test-case-row__name">{name}</label>
            </Col>
            <Col span={6}><Button icon="profile"/></Col>
            <Col span={6}><Button icon="delete"/></Col>
          </Row>
        </Card>
      </div>
    );
  }
}

export default TestClassRow;