import React, {Component} from "react";
import TestProjectHeader from "../TestProjectHeader/component";
import TestCaseList from "../TestCaseList/component";
import axios from "axios";

class TestProject extends Component {
  constructor(props) {
    super(props);
    this.state = {testCases: []};
  }

  fetchTestCases() {
    axios.get("http://localhost:3000/test_cases")
      .then(response => this.setState({testCases: response.data.data}));
  }

  componentWillMount() {
    this.fetchTestCases();
  }

  render() {
    const {name, description} = this.props;
    return (
      <div className="test-project">
        <TestProjectHeader name={name} description={description} testCount={this.state.testCases.length}/>
        <TestCaseList className="test-cases" testCases={this.state.testCases}/>
      </div>
    );
  }
}

export default TestProject;