import { createStore, applyMiddleware } from "redux";
import createSageMiddleware from "redux-saga";
import createLogger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

const sagaMiddleware = createSageMiddleware();
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
)