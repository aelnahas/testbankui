import React from "react";
import {Route} from "react-router-dom";
import TestProjectContainer from "./modules/testproject/components";

const routes = [
  {
    path: "/",
    component: TestProjectContainer,
    props: {
      name: "Test Project",
      description: "Some test project"
    }
  }
];


const RouteWithSubRoutes = route => (
  < Route
    path={route.path}
    render={props => (<route.component {...route.props} routes={route.subRoutes}/>)}
  />
);


const AppRoutes = () => (
  <div>
    {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
  </div>
);

export default AppRoutes;
export {routes, RouteWithSubRoutes} ;