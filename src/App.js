import React, { Component } from 'react';
import "./styles/css/App.css"
import {routes, RouteWithSubRoutes} from "./routes";
import { Layout } from "antd";
import { BrowserRouter } from "react-router-dom";

const {Header, Content, Footer} = Layout;

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Layout theme="light">
            <Header className="app-header" theme="light">
              App Header
            </Header>
            <div>
              <Layout>
                <Content className="app-content">
                  {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route}/>)}
                </Content>
              </Layout>
            </div>
          </Layout>
          <Footer> App Footer</Footer>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
